<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');

Route::group(['prefix' => 'api'], function(){

    Route::get('/getMoviesGenres', 'MoviesController@getGenres');
    Route::get('/getRandomMovieByGenre/{genre_id}', 'MoviesController@getRandomMovieByGenre');
    Route::get('/getRandomMovie', 'MoviesController@getRandomMovie');

    Route::get('/getTvGenres', 'TvController@getTvGenres');
    Route::get('/getRandomTvByGenre/{genre_id}', 'TvController@getRandomTvByGenre');
    Route::get('/getRandomTv', 'TvController@getRandomTv');

});
