<?php

namespace App\Http\Controllers;

use App\Classes\Video;

class MoviesController extends Controller {

    /**
     * @var Video
     */
    private $video;

    /**
     * MoviesController constructor.
     * @param Video $video
     */
    public function __construct( Video $video )
    {
        $this->video = $video;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getGenres()
    {
        return $this->video->genres();
    }

    /**
     * @param null $movie_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMovie($movie_id = null){
        return $this->video->movie($movie_id);
    }

    /**
     * @param null $genre_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRandomMovieByGenre($genre_id = null){
        return $this->video->randomMovieByGenre($genre_id);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRandomMovie(){
        return $this->video->randomMovie();
    }


}
