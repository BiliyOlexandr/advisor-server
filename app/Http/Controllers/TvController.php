<?php

namespace App\Http\Controllers;

use App\Classes\Tv;

class TvController extends Controller {

    /**
     * @var Tv
     */
    private $tv;

    /**
     * TvController constructor.
     * @param Tv $tv
     */
    public function __construct( Tv $tv )
    {
        $this->tv= $tv;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTvGenres(){
        return $this->tv->genres();
    }

    /**
     * @param null $tv_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTV($tv_id = null){
        return $this->tv->tv($tv_id);
    }

    /**
     * @param null $genre_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRandomTvByGenre($genre_id = null){
        return $this->tv->randomTvByGenre($genre_id);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRandomTv(){
        return $this->tv->randomTv();
    }
}
