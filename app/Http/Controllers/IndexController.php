<?php


namespace App\Http\Controllers;


use App\Classes\Video;

class IndexController
{

    /**
     * @var Video
     */
    private $video;

    /**
     * MoviesController constructor.
     * @param Video $video
     */
    public function __construct( Video $video )
    {
        $this->video = $video;
    }
    /**
     * Get Main page of Advisor service
     */
    public function index()
    {
        $popular = $this->video->movies->getPopular();

        foreach ($popular as $movie)
        {
            $image = $movie->getPosterImage();
            echo ($this->video->helper->getHtml($image, 'w154', 260, 420));
        }
    }
}
