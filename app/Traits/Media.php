<?php

namespace App\Traits;

use Illuminate\Support\Facades\Cache;

trait Media {

    public function сast($object){
        $counter = 0;
        $cast = [];
        $object->getCredits()->getCast()->map(function ($key, $item) use (&$cast, &$counter) {
            if ($counter <= 5) {
                $original_name = $item->getName();
                $cast[] = $original_name;

            }
            $counter++;
        });
        return array($cast);
    }
}
