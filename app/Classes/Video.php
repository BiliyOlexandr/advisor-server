<?php


namespace App\Classes;

use Tmdb\Model\Query\Discover\DiscoverMoviesQuery;
use Tmdb\Repository\DiscoverRepository;
use Illuminate\Support\Facades\Cache;
use Tmdb\Repository\GenreRepository;
use Tmdb\Repository\MovieRepository;
use Tmdb\Helper\ImageHelper;
use App\Traits\Media;

class Video
{
    const LANGUAGE                      = 'ru-RU';
    const DATE_FORMAT                   = 'Y';
    const IMG_PREFIX                    = 'https:';
    const SORT_BY                       = 'popularity.desc';
    const VOTE_AVG                      = 6.0;
    const GENERAL_COUNTER               = 'MOVIE_TOTAL_PAGES';
    const GENERAL_GENRE_PREFIX_COUNTER  = 'MOVIE_GENRE_';

    use Media;
    /**
     * @var MovieRepository
     */
    public $movies;
    /**
     * @var ImageHelper
     */
    public $helper;
    /**
     * @var GenreRepository
     */
    public $genres;
    /**
     * @var DiscoverRepository
     */
    public $discover;
    /**
     * @var Translate
     */
    public $translate;


    /**
     * Video constructor.
     * @param DiscoverRepository $discover
     * @param MovieRepository $movies
     * @param GenreRepository $genres
     * @param ImageHelper $helper
     * @param Translate $translate
     */
    public function __construct(
        DiscoverRepository $discover,
        MovieRepository $movies,
        GenreRepository $genres,
        ImageHelper $helper,
        Translate $translate
    )
    {
        $this->movies = $movies;
        $this->helper = $helper;
        $this->genres = $genres;
        $this->discover = $discover;
        $this->translate = $translate;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function genres(){
        $list = [];
        $this->genres->loadMovieCollection([
            'language' => self::LANGUAGE
        ])->map(function ($key, $item) use (&$list) {
            $list[] = [
                'id' => $item->getId(),
                'name' => mb_convert_case(mb_strtolower($item->getName(), 'UTF-8'), MB_CASE_TITLE, "UTF-8")
            ];
        });
        return response()->json([
            'genres' => $list
        ], 200);
    }

    /**
     * @param null $movie_id
     * @param null $genre_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function movie($movie_id = null, $genre_id = null){
        try{
            $movie = $this->movies->load($movie_id, [
                'append_to_response' => 'credits',
                'language' => self::LANGUAGE
            ]);
        } catch (\Exception $e){
            return response()->json([
                'success' => false,
                'message' => 'Movie not found'
            ], 400);
        }

        $response = [
            'id'                => $movie->getId(),
            'title'             => $movie->getTitle(),
            'original_title'    => $movie->getOriginalTitle(),
            'poster'            => $this->getPoster($movie),
            'backdrop'          => $this->getBackdrop($movie),
            'release_date'      => $this->getReleaseDate($movie),
            'overview'          => $this->getOverview($movie),
            'vote_average'      => $movie->getVoteAverage(),
            'cast'              => $this->getCast($movie),
            'genres'            => $this->getVideoGenres($movie),
            'director'          => $this->getDirector($movie),
            'country'           => ''
        ];

        $validate = \Validator::make($response, [
            'id'                => 'required',
            'title'             => 'required',
            'original_title'    => 'required',
            'poster'            => 'required',
            'release_date'      => 'required',
            'overview'          => 'required',
            'cast'              => 'required|array',
            'genres'            => 'required|array',
            'director'          => 'required|string'
        ]);

        if($validate->fails()) {
            if(!$genre_id){
                return $this->randomMovie();
            }
            return $this->randomMovieByGenre($genre_id);
        }

        if(!$this->isRussian($response['title'])){
            $response['title'] = $this->translate->translate($response['title'], $movie->getOriginalLanguage(), 'ru');
        }
        if(!$this->isRussian($response['overview'])){
            $response['overview'] = $this->translate->translate($response['overview'], $movie->getOriginalLanguage(), 'ru');
        }

        return response()->json($response, 200);
    }

    /**
     * @param null $genre_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function randomMovieByGenre($genre_id = null){
        if(!$genre_id){
            return response()->json([
                'success' => false,
                'message' => 'genre_id is required'
            ], 400);
        }

        if(!Cache::has(self::GENERAL_GENRE_PREFIX_COUNTER.$genre_id)){
            $query = $this->movieQueryBuilder($genre_id);
            $res = $this->discover->discoverMovies($query);
            Cache::forever(self::GENERAL_GENRE_PREFIX_COUNTER.$genre_id, (int)$res->getTotalPages());
        }

        $max_page = Cache::get(self::GENERAL_GENRE_PREFIX_COUNTER.$genre_id);

        $page = rand(1, $max_page);
        $query = $this->movieQueryBuilder($genre_id, $page);
        $movie_id = $this->randomVideoFromScope($query);
        if($movie_id){
            return $this->movie($movie_id, $genre_id);
        }
        return response()->json([
            'success' => false,
            'message' => 'Video not found'
        ], 200);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function randomMovie(){
        if(!Cache::has(self::GENERAL_COUNTER)){
            $query = $this->movieQueryBuilder();
            $res = $this->discover->discoverMovies($query);
            Cache::forever(self::GENERAL_COUNTER, (int)$res->getTotalPages());
        }

        $max_page = Cache::get(self::GENERAL_COUNTER);
        $page = rand(1, $max_page);

        $query = $this->movieQueryBuilder('', $page);
        $movie_id = $this->randomVideoFromScope($query);

        if($movie_id){
            return $this->movie($movie_id);
        }

        return response()->json([
            'success' => false,
            'message' => 'Video not found'
        ], 200);
    }

    /**
     * @param \Tmdb\Model\AbstractModel|null $movie
     * @return mixed|string
     */
    public function getDirector(?\Tmdb\Model\AbstractModel $movie)
    {
        $director = '';
        $movie->getCredits()->getCrew()->map(function ($key, $item) use (&$director) {
            if ($item->getJob() == 'Director') {
                $director = $item->getName();
            }
        });

        return $director;
    }

    /**
     * @param \Tmdb\Model\AbstractModel|null $movie
     * @return array
     */
    public function getCast(?\Tmdb\Model\AbstractModel $movie): array
    {
        return $this->сast($movie);
    }

    /**
     * @param \Tmdb\Model\AbstractModel|null $movie
     * @return array
     */
    public function getVideoGenres(?\Tmdb\Model\AbstractModel $movie): array
    {
        $genres = [];
        $movie->getGenres()->map(function ($key, $item) use (&$genres) {
            $genres[] = $item->getName();
        });
        return $genres;
    }

    /**
     * @param \Tmdb\Model\AbstractModel|null $movie
     * @return string
     */
    public function getPoster(?\Tmdb\Model\AbstractModel $movie): string
    {
        return $movie->getPosterImage()->getFilePath()
            ?
                self::IMG_PREFIX . $this->helper->getUrl($movie->getPosterImage(), 'w780')
            :
                '';
    }

    /**
     * @param \Tmdb\Model\AbstractModel|null $movie
     * @return string
     */
    public function getBackdrop(?\Tmdb\Model\AbstractModel $movie): string
    {
        return $movie->getBackdropImage()->getFilePath()
            ?
                self::IMG_PREFIX . $this->helper->getUrl($movie->getBackdropImage(), 'w780')
            :
                '';
    }

    /**
     * @param \Tmdb\Model\AbstractModel|null $movie
     * @return mixed
     */
    public function getReleaseDate(?\Tmdb\Model\AbstractModel $movie)
    {
        return $movie->getReleaseDate()->format(self::DATE_FORMAT);
    }

    /**
     * @param \Tmdb\Model\AbstractModel|null $movie
     * @return string
     */
    public function getOverview(?\Tmdb\Model\AbstractModel $movie): string
    {
        $text = strip_tags($movie->getOverview());
        $text = str_replace('\\r', '', $text);
        $text = str_replace('\\n', '', $text);
        $text = str_replace('\\', '', $text);
        return $text;
    }

    /**
     * @param string $genre_id
     * @param int $page
     * @return DiscoverMoviesQuery
     */
    public function movieQueryBuilder($genre_id = '', $page = 1): DiscoverMoviesQuery
    {
        $query = new DiscoverMoviesQuery();
        $query->language(self::LANGUAGE)
            ->page($page)
            ->certificationCountry('RU')
            ->certificationLte('')
            ->voteAverageGte(self::VOTE_AVG)
            ->withGenres($genre_id)
            ->sortBy(self::SORT_BY);
        return $query;
    }

    /**
     * @param DiscoverMoviesQuery $query
     * @return int
     */
    public function randomVideoFromScope(DiscoverMoviesQuery $query): int
    {
        $res = $this->discover->discoverMovies($query)->getAll();
        $rand_element = rand(1, count($res));
        $counter = 0;
        $movie_id = 0;

        foreach ($res as $k => $item) {
            $counter++;
            if ((int)$counter === (int)$rand_element) {
                $movie_id = $item->getId();
                break;
            }
        }
        return $movie_id;
    }

    /**
     * @param $text
     * @return false|int
     */
    public function isRussian($text) {
        return preg_match('/[А-Яа-яЁё]/u', $text);
    }

}
