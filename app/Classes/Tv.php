<?php


namespace App\Classes;


use Tmdb\Model\Query\Discover\DiscoverTvQuery;
use Tmdb\Repository\DiscoverRepository;
use Illuminate\Support\Facades\Cache;
use Tmdb\Repository\GenreRepository;
use Tmdb\Repository\TvRepository;
use Tmdb\Helper\ImageHelper;
use App\Traits\Media;

class Tv
{
    const LANGUAGE                      = 'ru-RU';
    const DATE_FORMAT                   = 'Y';
    const IMG_PREFIX                    = 'https:';
    const SORT_BY                       = 'popularity.desc';
    const VOTE_AVG                      = 6.0;
    const GENERAL_COUNTER               = 'TV_TOTAL_PAGES';
    const GENERAL_GENRE_PREFIX_COUNTER  = 'TV_GENRE_';

    use Media;

    /**
     * @var TvRepository
     */
    public $tv;
    /**
     * @var ImageHelper
     */
    public $helper;
    /**
     * @var GenreRepository
     */
    public $genres;
    /**
     * @var DiscoverRepository
     */
    public $discover;
    /**
     * @var Translate
     */
    public $translate;

    /**
     * Tv constructor.
     * @param DiscoverRepository $discover
     * @param TvRepository $tv
     * @param GenreRepository $genres
     * @param ImageHelper $helper
     * @param Translate $translate
     */
    public function __construct(
        DiscoverRepository $discover,
        TvRepository $tv,
        GenreRepository $genres,
        ImageHelper $helper,
        Translate $translate
    )
    {
        $this->tv = $tv;
        $this->helper = $helper;
        $this->genres = $genres;
        $this->discover = $discover;
        $this->translate = $translate;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function genres()
    {
        $list = [];
        $this->genres->loadTvCollection([
            'language' => self::LANGUAGE
        ])->map(function($key, $item)use(&$list){
            $list[] = [
                'id' => $item->getId(),
                'name' => mb_convert_case(mb_strtolower($item->getName(), 'UTF-8'), MB_CASE_TITLE, "UTF-8")
            ];
        });
        return response()->json([
            'genres' => $list
        ] ,200);
    }

    /**
     * @param null $tv_id
     * @param null $genre_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function tv($tv_id = null, $genre_id = null){
        try{
            $tv = $this->tv->load($tv_id, [
                'append_to_response' => 'credits',
                'language' => self::LANGUAGE
            ]);
        } catch (\Exception $e){
            return response()->json([
                'success' => false,
                'message' => 'TV not found'
            ], 400);
        }

        $response = [
            'id'                => $tv->getId(),
            'name'              => $tv->getName(),
            'original_name'     => $tv->getOriginalName(),
            'poster'            => $this->getPoster($tv),
            'backdrop'          => $this->getBackdrop($tv),
            'first_air_date'    => $this->dateFormat($tv->getFirstAirDate()),
            'last_air_date'     => $this->dateFormat($tv->getlastAirDate()),
            'overview'          => $this->getOverview($tv),
            'genres'            => $this->getGenres($tv),
            'vote_average'      => $tv->getVoteAverage(),
            'cast'              => $this->getCast($tv),
            'country'           => ''
        ];

        $validate = \Validator::make($response, [
            'id'                => 'required',
            'name'              => 'required',
            'original_name'     => 'required',
            'poster'            => 'required',
            'first_air_date'    => 'required',
            'overview'          => 'required',
            'cast'              => 'required|array',
            'genres'            => 'required|array'
        ]);

        if($validate->fails()) {
            if(!$genre_id){
                return $this->randomTv();
            }
            return $this->randomTvByGenre($genre_id);
        }

        if(!$this->isRussian($response['name'])){
            $response['name'] = $this->translate->translate($response['name'], $tv->getOriginalLanguage(), 'ru');
        }
        if(!$this->isRussian($response['overview'])){
            $response['overview'] = $this->translate->translate($response['overview'], $tv->getOriginalLanguage(), 'ru');
        }

        return response()->json($response, 200);
    }

    /**
     * @param null $genre_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function randomTvByGenre($genre_id = null)
    {
        if(!$genre_id){
            return response()->json([
                'success' => false,
                'message' => 'genre_id is required'
            ], 400);
        }

        if(!Cache::has(self::GENERAL_GENRE_PREFIX_COUNTER.$genre_id)){
            $query = $this->tvQueryBuilder($genre_id);
            $res = $this->discover->discoverTv($query);
            Cache::forever(self::GENERAL_GENRE_PREFIX_COUNTER.$genre_id, (int)$res->getTotalPages());
        }

        $max_page = Cache::get(self::GENERAL_GENRE_PREFIX_COUNTER.$genre_id);
        $page = rand(1, $max_page);

        $query = $this->tvQueryBuilder($genre_id, $page);
        $tv_id = $this->randomTvFromScope($query);

        if($tv_id){
            return $this->tv($tv_id, $genre_id);
        }
        return response()->json([
            'success' => false,
            'message' => 'TV not found'
        ], 200);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function randomTv()
    {
        if(!Cache::has(self::GENERAL_COUNTER)){
            $query = $this->tvQueryBuilder();
            $res = $this->discover->discoverTv($query);
            Cache::forever(self::GENERAL_COUNTER, (int)$res->getTotalPages());
        }

        $max_page = Cache::get(self::GENERAL_COUNTER);
        $page = rand(1, $max_page);

        $query = $this->tvQueryBuilder('', $page);
        $tv_id = $this->randomTvFromScope($query);
        if($tv_id){
            return $this->tv($tv_id);
        }

        return response()->json([
            'success' => false,
            'message' => 'Video not found'
        ], 200);
    }

    /**
     * @param \Tmdb\Model\AbstractModel|null $tv
     * @return string
     */
    public function getPoster(?\Tmdb\Model\AbstractModel $tv): string
    {
        return $tv->getPosterImage()->getFilePath()
            ?
                self::IMG_PREFIX . $this->helper->getUrl($tv->getPosterImage(), 'w780')
            :
                '';
    }

    /**
     * @param \Tmdb\Model\AbstractModel|null $tv
     * @return string
     */
    public function getBackdrop(?\Tmdb\Model\AbstractModel $tv): string
    {
        return $tv->getBackdropImage()->getFilePath()
            ?
                self::IMG_PREFIX . $this->helper->getUrl($tv->getBackdropImage(), 'w780')
            :
                '';
    }

    /**
     * @param \Tmdb\Model\AbstractModel|null $tv
     * @return array
     */
    public function getGenres(?\Tmdb\Model\AbstractModel $tv): array
    {
        $genres = [];
        $tv->getGenres()->map(function ($key, $item) use (&$genres) {
            $genres[] = $item->getName();
        });
        return $genres;
    }

    /**
     * @param \Tmdb\Model\AbstractModel|null $tv
     * @return array
     */
    public function getCast(?\Tmdb\Model\AbstractModel $tv): array
    {
        return $this->сast($tv);
    }

    /**
     * @param string $genre_id
     * @param int $page
     * @return DiscoverTvQuery
     */
    public function tvQueryBuilder($genre_id = '', $page = 1): DiscoverTvQuery
    {
        $query = new DiscoverTvQuery();
        $query->language(self::LANGUAGE)
            ->page($page)
            ->voteAverageGte(self::VOTE_AVG)
            ->withGenres($genre_id)
            ->sortBy(self::SORT_BY);
        return $query;
    }

    /**
     * @param DiscoverTvQuery $query
     * @return int
     */
    public function randomTvFromScope(DiscoverTvQuery $query): int
    {
        $res = $this->discover->discoverTv($query)->getAll();
        $rand_element = rand(1, count($res));
        $counter = 0;
        $tv_id = 0;

        foreach ($res as $k => $item) {
            $counter++;
            if ((int)$counter === (int)$rand_element) {
                $tv_id = $item->getId();
                break;
            }
        }
        return $tv_id;
    }

    /**
     * @param $date
     * @return mixed
     */
    public function dateFormat($date){
        return $date->format(self::DATE_FORMAT);
    }

    /**
     * @param \Tmdb\Model\AbstractModel|null $tv
     * @return mixed
     */
    public function getOverview(?\Tmdb\Model\AbstractModel $tv)
    {
        $text = strip_tags($tv->getOverview());
        $text = str_replace('\r', '', $text);
        $text = str_replace('\n', '', $text);
        $text = str_replace('\\', '', $text);
        return $text;
    }

    /**
     * @param $text
     * @return false|int
     */
    public function isRussian($text) {
        return preg_match('/[А-Яа-яЁё]/u', $text);
    }
}
