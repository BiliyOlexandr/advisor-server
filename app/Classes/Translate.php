<?php


namespace App\Classes;

use Statickidz\GoogleTranslate;

class Translate
{
    const LANG_FROM = 'en';
    const LANG_TO = 'ru';

    /**
     * @var GoogleTranslate
     */
    public $trans;

    public function __construct()
    {
        $this->trans = new GoogleTranslate();
    }

    /**
     * @param $text
     * @param string $from
     * @param string $to
     * @return string
     */
    public function translate($text, $from = self::LANG_FROM, $to = self::LANG_TO)
    {
        return $this->trans::translate($from, $to, $text);
    }
}
